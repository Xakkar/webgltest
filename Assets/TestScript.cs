﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScript : MonoBehaviour
{
    private AudioSource sound;
    private List<string> myAudio;
    private int x;

    private void Awake()
    {
        x = 0;
        sound = GetComponent<AudioSource>();
        myAudio = new List<string>();

        myAudio.Add("../assets/audio/amb_birds_1.mp3");
        myAudio.Add("../assets/audio/amb_birds_2.mp3");
        myAudio.Add("../assets/audio/amb_birds_3.mp3");
        myAudio.Add("../assets/audio/amb_birds_4.mp3");
        myAudio.Add("../assets/audio/amb_birds_5.mp3");
        myAudio.Add("../assets/audio/amb_birds_6.mp3");
        myAudio.Add("../assets/audio/amb_birds_7.mp3");
        myAudio.Add("../assets/audio/amb_city_in.mp3");
        myAudio.Add("../assets/audio/amb_city_loop.mp3");
        myAudio.Add("../assets/audio/amb_city_out.mp3");
        myAudio.Add("../assets/audio/amb_forest_in.mp3");
        myAudio.Add("../assets/audio/amb_forest_loop.mp3");
        myAudio.Add("../assets/audio/amb_forest_out.mp3");
        myAudio.Add("../assets/audio/bell_alarm.mp3");
        myAudio.Add("../assets/audio/bridge_down.mp3");
        myAudio.Add("../assets/audio/build_complete.mp3");
        myAudio.Add("../assets/audio/button_click.mp3");
        myAudio.Add("../assets/audio/city_building_click.mp3");
        myAudio.Add("../assets/audio/click_castle_build.mp3");
        myAudio.Add("../assets/audio/click_infrastructure_build.mp3");
        myAudio.Add("../assets/audio/click_resource_build.mp3");
        myAudio.Add("../assets/audio/click_temple_build.mp3");
        myAudio.Add("../assets/audio/click_war_build.mp3");
        myAudio.Add("../assets/audio/epic_short.mp3");
        myAudio.Add("../assets/audio/epic_theme_v2.mp3");
        myAudio.Add("../assets/audio/flut_city.mp3");
        myAudio.Add("../assets/audio/flut_city2.mp3");
        myAudio.Add("../assets/audio/magic_ritual.mp3");
        myAudio.Add("../assets/audio/quest_award.mp3");
        myAudio.Add("../assets/audio/soldiers_marching_loop.mp3");
        myAudio.Add("../assets/audio/swords_hit.mp3");
        myAudio.Add("../assets/audio/theme_relax.mp3");
        myAudio.Add("../assets/audio/window_appear.mp3");
        myAudio.Add("../assets/audio/window_slide.mp3");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            if (x >= myAudio.Count)
                x = 0;
            PlaySound(myAudio[x]);
            x++;
        }
    }

    IEnumerator StartSound(string str)
    {
        WWW www = new WWW(str);
        yield return www;
        sound.clip = www.GetAudioClip(true, false, AudioType.MPEG);
        sound.Play();
    }

    public void PlaySound(string str)
    {
        sound.loop = false;
#if UNITY_WEBGL
        StartCoroutine(StartSound(str));
#endif
    }
}
